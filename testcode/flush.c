#include <stdio.h>
#include <unistd.h>
#include <time.h>

void progressbar(int i, int max){
	int percent = (i * 100) / max;
	char bar[13] = "|          |";
	int inc = (percent / 10) % 10;
	if (i == max) inc = 10;
    

	int j;
	for(int j = 0; j < inc; j++){
		if(percent > 1) bar[j + 1] = '-';
	}

	printf("%s %d %%",bar,percent);
	
	if(i == max){
		printf("\n");
	} else {
		printf("\r");
		fflush(stdout);
	}
}

int main(void){
	int i = 0;
	for(i = 0; i <= 33; i++){
		progressbar(i,33);
		sleep(1);
	}
	return 0;
}
