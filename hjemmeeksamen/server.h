#include <netinet/in.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <signal.h>

struct sockaddr_in serveraddr, clientaddr;

/* Data structure for client list*/
typedef struct Client{
	struct Client *next;
	char ipstr[INET6_ADDRSTRLEN]; // Readable IP
	int socket;                   // Socket
	char wd[4096];                // Clients current directory
}client; 
