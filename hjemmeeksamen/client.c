#include "client.h"
#include "common.h"

/*
 * 				      client 
 *
 * Sends and receives messages using the application
 * layer protocol. Receives commands and strings.
 *
 */

/*       Global variables           */
int sock; // Socket
char ipstr[INET6_ADDRSTRLEN];
char *ipver;
char *returnmsg;
/*----------------------------------*/

/*       
 * Establishes connection and returns
 * the socket.
 *
 */
int getsocket(char *hostname, char *port){
	int status;
	struct addrinfo hints, *res;
	void *addr;

	/* Sets filters for TCP and IP  */
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;       /* Unspecified if IPv4 & IPv6 */
	hints.ai_socktype = SOCK_STREAM;   /* TCP stream */

	/* Checks if it can find specified address  */
	if((status = getaddrinfo(hostname, port, &hints, &res)) != 0){
		fprintf(stderr, "Error getting address from hostname: %s\n", gai_strerror(status));
		return 1;
	}

	/* Sets socket*/
	sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol); 
	
	/* Checks for IPv4 or IPv6 */
	if(res->ai_family == AF_INET){
		struct sockaddr_in *ipv4 = (struct sockaddr_in *)res->ai_addr;
		addr = &(ipv4->sin_addr);
		ipver = "IPv4";
	} else {
		struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)res->ai_addr;
		addr = &(ipv6->sin6_addr);
		ipver = "IPv6";
	}

	/* Gets IP in human readable form */
	inet_ntop(res->ai_family,addr,ipstr,sizeof(ipstr));

	/* Checks connection */
	if(connect(sock,res->ai_addr, res->ai_addrlen)){
		fprintf(stderr, "Error connecting to %s:%s (%s): %s\n",ipstr, port, ipver, strerror(errno));
		exit(EXIT_FAILURE);
	} else {
		printf("Connection to %s:%s (%s)\n",ipstr,port,ipver);
	}

	freeaddrinfo(res);

	return sock;
}

int main(int argc, char *argv[]){
	char inputstr[4096];
	int sock, filetype, status, i;

	if(argc != 3){
		fprintf(stderr,"Usage: ./client hostname port");
		return 0;
	}

	sock = getsocket(argv[1], argv[2]); // Connects and returns socket


	/*               Main loop                  */
	while(1){

		/*                       Menu                         */
		printf("File browser @ %s\n",ipstr);
		printf("[1] Print current working directory\n");
		printf("[2] List files of current working directory\n");
		printf("[3] Print file information\n");
		printf("[4] Change directory\n");
		printf("[5] Print file\n");
		printf("[exit] Close connection and exit program\n");

		printf("%s > ", argv[1]);
		fgets(inputstr,MAX_MSG,stdin);
		strtok(inputstr,"\n");

		/*             Exit             */
		if(strcmp(inputstr,"exit") == 0){
			sndmsg(sock, 0,"",0);
			break;

		/*              ls                 */
		} else if(strcmp(inputstr,"2") == 0){
			sndmsg(sock, 2,"",0);
			readmsg(sock);
			printf("%s\n",returnmsg);
			free(returnmsg);

		/*      print current directory     */
		} else if(strcmp(inputstr,"1") == 0){
			sndmsg(sock, 1, "", 0);
			readmsg(sock);
			printf("%s\n",returnmsg);
			free(returnmsg);

		/*      Print file information      */
		} else if(strcmp(inputstr,"3") == 0){
			sndmsg(sock,3,"",0);
			printf("Enter file name: ");
			fgets(inputstr,MAX_MSG,stdin);
			strtok(inputstr,"\n");
			sndmsg(sock, 3, inputstr, strlen(inputstr));

			filetype = readmsg(sock); // Gets msg with filetype as return value
			if(filetype == FILENOTFOUND) printf("Error: File not found\n");
			if(filetype == DIRECTORY) printf("File is a directory\n");
			if(filetype == REGFILE) printf("File is a regular file\n");
			if(filetype == SPECFILE) printf("File is a special file\n");
			if(filetype == SYMLINK) printf("File is a symbolic link\n");
			free(returnmsg);

		/*      Change directory           */
		} else if(strcmp(inputstr,"4") == 0){
			sndmsg(sock, 4, "", 0);
			/*              Submenu                 */
			printf("..     - Change directory to parent directory\n");
			printf("/<dir> - Change absolute directory (Relative to start directory restriction)\n");
			printf(" <dir> - Change directory relative to current\n");
			printf("New directory: ");
			fgets(inputstr,MAX_MSG,stdin);
			strtok(inputstr,"\n");
			sndmsg(sock,4,inputstr,strlen(inputstr));

			/*      Gets status based on return value of read     */
			status = readmsg(sock);
			if(status == 0) printf("Changed folder to: %s\n",inputstr);
			else if(status == 2) printf("Already at top folder\n");
			else printf("%s: Not a directory\n",inputstr);
			free(returnmsg);

		/*    Prints regular files      */
		} else if(strcmp(inputstr,"5") == 0){
			printf("Print file: ");
			fgets(inputstr,MAX_MSG,stdin);
			strtok(inputstr,"\n");
			sndmsg(sock, 5, inputstr, strlen(inputstr));
			filetype = readmsg(sock);

			/*  Prints file and replaces unprintable characters with '.' */
			if(filetype == REGFILE){
				for(i = 0; i < strlen(returnmsg);i++){
					if(isprint(returnmsg[i]) || (returnmsg[i] == '\n')) printf("%c",returnmsg[i]);
					else printf(".");
				}
				printf("%s\n",returnmsg);

			} else { // Error messages if unprintable
				if(filetype == FILENOTFOUND) printf("Error: File not found\n");
				if(filetype == DIRECTORY) printf("File is a directory\n");
				if(filetype == SPECFILE) printf("File is a special file\n");
				if(filetype == SYMLINK) printf("File is a symbolic link\n");
			}
			free(returnmsg);
		} else {
			printf("%s: not a command\n",inputstr);
		}
		printf("\nPress Return to continue\n");
		fgets(inputstr,MAX_MSG,stdin);
	}
	close(sock);
	return 0;
}
