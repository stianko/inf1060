#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#define DIRECTORY 1
#define REGFILE 2
#define SYMLINK 3
#define FILENOTFOUND 4
#define SPECFILE 0

#define MAX_MSG 4095


int sndmsg(int sock, char command, char *msg, int total_str_len);
int readmsg(int sock);

