/*
 *
 */

#include "server.h"
#include "common.h"

char *returnmsg;
char ipstr[INET6_ADDRSTRLEN];
char *msg;

client *head;

/*
 * Executes command with popen, which returns a stream
 * of the shell-result. This is stored in a dynamically
 * allocated variable (msg).
 *
 */
int runcmd(char *cmd){
	FILE *in;
	extern FILE *popen();
	char bufc;
	msg = malloc(MAX_MSG);
	void *tmppointer;
	int size = 0;

	if((in = popen(cmd, "r"))){
		printf("Running cmd \"%s\"\n",cmd);
		
		/* Loops through each char of the stream*/
		while(1){
			bufc = fgetc(in);
			if((size % MAX_MSG) == 0 && size > 0){
#ifdef DEBUG
				printf("Reallocing to size: %d\n",size + MAX_MSG);
#endif
				/* Reallocs every 4095 bytes */
				if(!(tmppointer = realloc(msg, size + MAX_MSG))) perror ("Realloc"); 
				msg = tmppointer;
			}
			if(bufc == EOF){ // End case
				msg[size] = '\0';
				break;
			}
			msg[size] = bufc;						
			size++;
		}
		pclose(in);
	} else {
		return 1;
	}
	return 0;
}

/* Adds an element to the linked list of clients */
int add_client(int sock, char* ip){
	client *this = (client *) malloc(sizeof(client));
	this->socket = sock;                // Sets socket
	getcwd((char *)&this->wd, MAX_MSG); // Sets working directory
	strcpy((char *)&this->ipstr,ip);    // Sets IP (for use in server prints)
	this->next = head->next;
	head->next = this;
	return 0;
}

/* Returns client with socket specified in parameter */
client *get_client(int sock){
	client *this = head->next;
	while(1){
		if(this == NULL) return 0;
		if(this->socket == sock) return this;
		this = this->next;
	}
	return 0;
}

/* Removes an element from the linked list of clients */
int remove_client(int sock){
	client *this = head->next;
	client *prev = head;
	while(1){
		if(this->socket == sock){
			printf("%s disconnected\n",this->ipstr);
			prev->next = this->next;
			free(this);
			return sock;
		}
		prev = this;
		this = this->next;
	}
	return 0;
}

/* 
 * Gets fileinfo via lstat
 * Returns integer defined with macros in common header file
 */
int getfileinfo(char *filename){
	printf("getfileinfo: %s\n",filename);
	struct stat sb;
	int status;
	
	if((status = lstat(filename, &sb)) == -1) return FILENOTFOUND;

	switch (sb.st_mode & S_IFMT) {
		case S_IFDIR: return DIRECTORY;
		case S_IFREG: return REGFILE;
		case S_IFLNK: return SYMLINK;
		default: return 0;
	}
	
}
/* Changes working directory of a client specified in parameters. */
int ch_dir(int sock, char *directory){
	char tmp[MAX_MSG];
	getcwd((char *)&tmp,MAX_MSG); // Current directory of server

	/* Changing to parent folder */
	if(strcmp(directory,"..") == 0){
		// Already at top folder
		if(strcmp((char *)&get_client(sock)->wd,(char *)&tmp) == 0) return 2;
		int i = 0;
		int position = 0;
		char delimiter = '/';
		while(i < strlen(get_client(sock)->wd)){
			if(delimiter == get_client(sock)->wd[i]){
				position = i;
			}
			i++;
		}
		get_client(sock)->wd[position] = '\0';
		printf("New dir: %s Final / at index %d\n",directory, position);
		return 0;
	}

	/* If command starts with / change absolute directory (relative to starting directory) */
	if(directory[0] == '/'){
		strcat((char *)&tmp, directory);
		if(getfileinfo((char *)&tmp) == DIRECTORY){
			strcpy((char *)&get_client(sock)->wd,(char *)&tmp);
			return 0;
		} else return 1;
	}
	/* Changing folder to a folder relative to the current directory */
	if(getfileinfo(directory) == DIRECTORY){
		strcat((char *)&get_client(sock)->wd,"/");
		strcat((char *)&get_client(sock)->wd, directory);
		return 0;
	}
	return 1;
}
/* Frees allocated memory for client data structure */

void signalhandler(int signum){
	printf("Quitting\n");
	if(head->next != NULL){
		client *current = head->next;
		client *next;
		while(1){
			if(current == NULL) break;
			next = current->next;
			close(current->socket);
			free(current);
			current = next;
		}
	}
	free(head);
	exit(signum);
}

int main(int argc, char *argv[]){
	signal(SIGINT,signalhandler);
	head = (client *) malloc(sizeof(client));// Head for client linked list
	/* Initialize file descriptors for select() */
	fd_set main_fd_list;
	fd_set tmp;
	int fd_max;

	int request_sock;
	int stat;
	socklen_t clientaddrlen;
	struct sockaddr_storage addr;
	int port, i, j, sock, status;

	FD_ZERO(&main_fd_list);
	FD_ZERO(&tmp);

	/* Setting up server  */
	request_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	memset((void *) &serveraddr, 0, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = INADDR_ANY;
	serveraddr.sin_port = htons(atoi(argv[1]));

	bind(request_sock, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
	listen(request_sock, SOMAXCONN);
	FD_SET(request_sock, &main_fd_list);
	fd_max = request_sock;

	while(1){

		tmp = main_fd_list;
		if(select(fd_max + 1, &tmp, NULL, NULL, NULL) == -1){
			perror("select");
			exit(1);
		}

		// Iterate through connections
		for(i = 0; i <= fd_max; i++){
			if(FD_ISSET(i, &tmp)){ // New connection
				if(i == request_sock){
					clientaddrlen = sizeof(addr);
					sock = accept(request_sock,(struct sockaddr *)&clientaddr,&clientaddrlen);

					FD_SET(sock, &main_fd_list);
					if(sock > fd_max){
						fd_max = sock;
					}
					/* Gets human readable name of connected client */
					getpeername(sock, (struct sockaddr*)&addr, &clientaddrlen);
					struct sockaddr_in *s = (struct sockaddr_in *)&addr;
					port = ntohs(s->sin_port);
					inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);

					/* Adds client to data structure */
					add_client(sock,ipstr);

					printf("Incoming connection from %s:%d on socket: %d\n",ipstr,port,sock);
				} else {
					for(j = 0; j <= fd_max; j++){ // Iterates through sockets
						if(FD_ISSET(j,&main_fd_list)){ // If socket is set:
							if(j != request_sock && j == i){
								stat = readmsg(j); // Reads command from current client
								printf("%s running command: %d\n",get_client(j)->ipstr,stat);

								/* Client exits locally */
								if(stat == 0){
									remove_client(j);
									close(j);
									FD_CLR(j, &main_fd_list);
								}

								/* Client requests current working directory */
								if(stat == 1){
									printf("WD: %s\n", (char *)&get_client(j)->wd);
									sndmsg(j,1,(char *)&get_client(j)->wd,
											(int) strlen((char *)&get_client(j)->wd));
								}

								/* Client requests ls */
								if(stat == 2){
									char command[MAX_MSG];
									memset(command,0,MAX_MSG);
									strcat((char *)&command, "ls ");
									strcat((char *)&command, (char *)&get_client(j)->wd);
									runcmd(command);

									sndmsg(j,2,msg, (int) strlen(msg));
									free(msg);
								}
								/* Client requests file information */
								if(stat == 3){
									char command[MAX_MSG];
									memset(command,0,MAX_MSG);
									free(returnmsg);
									readmsg(j);
									strcpy((char *)&command,(char *)&get_client(j)->wd);
									strcat((char *)&command, "/");
									strcat((char *)&command, returnmsg);

									printf("%s\n",command);
									sndmsg(j,getfileinfo(command),"",0); // Fileinfo sent as integer in header
								}

								/* Client requests change of directory */
								if(stat == 4){
									free(returnmsg);
									readmsg(j);
									sndmsg(j,ch_dir(j,returnmsg),"",0); // Result sent as integer in header

								}
								/* Client requests to print a file */
								if(stat == 5){
									char command[MAX_MSG];
									memset(command,0,MAX_MSG);
									strcpy((char *)&command, "cat ");
									strcat((char *)&command, (char *)&get_client(j)->wd);
									strcat((char *)&command, "/");
									strcat((char *)&command, returnmsg);
									status = getfileinfo((char *)&command[4]);
									printf("%s.\n",(char *)&command[0]);

									if(status == REGFILE){ // If regular file, sends file
										runcmd(command);
										sndmsg(j,REGFILE,msg,(int) strlen(msg));
										free(msg);
									} else sndmsg(j,status,"",0); // If not reg. file, send error in header
								}
								free(returnmsg);
							} // End of client loop
						}
					}
				}
			} // End of new connection
		} // End of FD-loop
	} //End of main loop
	return 0;
}
