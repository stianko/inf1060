#include "alp.h"
#include "common.h"

/*        Application layer protocol
 * 
 * 		Header:
 *      +---+---+---+---+---+---+---+---+
 *      | a | b | b | b | c | c | c | c |
 *      +---+---+---+---+---+---+---+---+
 *      | c | c | c | c | c | c | c | c |
 *      +---+---+---+---+---+---+---+---+
 *
 *      a -> flag for more packages
 *      b -> command index
 *      c -> size of string (max 4096B)
 *
 */

// Counter for number of bytes sent.
int pcnt;
// Global variable for return message.
// Needs to be called free() on after readmsg gets a string.
char *returnmsg;

// Progressbar for showing bytes sent.
// Each call clears the current line.
void progressbar(int i, int max){
	int percent = (i * 100) / max;
	char bar[13] = "|          |";
	int inc = (percent / 10) % 10;
	if (i == max) inc = 10;
    

	int j;
	for(j = 0; j < inc; j++){
		if(percent > 1) bar[j + 1] = '-';
	}

	printf("%s %3d %%  | %8dB of %8dB sent",bar,percent,i,max);
	
	if(i == max) printf("\n");
	else printf("\r");
}

/*
 *          readmsg(int sock)
 * 
 * Reads incoming messages on socket specified in
 * parameter. Messages doesn't have any restrictions
 * in length, and read keeps reading as long as the
 * header flag is set ('a'). It's return value is the 
 * command specified as 'b' in the header.
 * The command is left up to the client/server to
 * interpret.
 *
 */
int readmsg(int sock){
	/* Variables used to decode the header */
	unsigned char len[2], leftb, command, flag;
	unsigned int len_int, len_int2;
	/* ----------------------------------- */

	int totalsize = 0; // Used to terminate string

	returnmsg = malloc(sizeof(char)); // Small malloc so that it can later be realloc'ed

	/*             read-loop               */
	while(1){
		read(sock,(void *)&len,2); // reads header

		/*
	 	*  Get parameters from header
	 	*/

		/* Bitwise operation to get flag (a) from header    */
		flag = len[0];
		flag >>= 7;

		/* Bitwise operation to get command (b) from header */
		command = len[0];
		command <<= 1;
		command >>= 5;

#ifdef DEBUG
		printf("Read command: %d\n",(int)command);
#endif
		/* Bitwise operation to get packet length from 'c' */
		leftb = len[0];
		leftb <<= 4;
		leftb >>= 4;
		len_int = leftb;
		len_int <<= 8;
		len_int2 = len[1];
		len_int |= len_int2;

#ifdef DEBUG
		printf("decoded header: len[0]: %d, len[1], %d\n",(int)len[0],(int)len[1]);
		printf("Receiving string of length %d\n",len_int);
#endif	
		if(len_int == 0) break; // Breaks if msg is only header

		totalsize += len_int;
		/* 
		 * Reallocs and copies data to returnmsg.
		 * Global variable that is used and freed
		 * in client/server.
		*/ 
		void *tmppointer = realloc(returnmsg, totalsize + 1);
		returnmsg = tmppointer;
		read(sock, (char *)&returnmsg[totalsize - len_int],len_int);

		returnmsg[totalsize] = '\0';
		
		if(flag == 0) break; // Breaks at final message
	}
	return (int) command;
}

/*
 *    sndmsg(int sock, char command, char *msg, int total_str_len)
 *
 *    Writes message of specified length with a maximum of 
 *    4096 bytes for each write.
 *    (Lenght is specified as parameter to print progress-
 *    bar of sends larger than 4000 bytes.
 *    Each write is composed of a 2 byte header.
 * 	  Calls itself recursively with rest of message.
 *
 */
int sndmsg(int sock, char command, char *msg, int total_str_len){

#ifdef DEBUG
	printf("sndmsg called with parameters:\nsocket:%d\ncommand:%d\nstrlen:%d\n",
			sock,(int) command, total_str_len);
#endif

	int len_int = strlen(msg);
	char flag = 0;

	/*
	*  Set parameters in header
	*/
	
	if(len_int >= MAX_MSG){ // Sets flag if length of msg is over max
		flag = 1;
		len_int = MAX_MSG;
	} 
	unsigned char tmp[len_int + 2],cmd_tmp;
	memset(&tmp, 0, 2);

	/* Bitwise operation to set length (c) in header    */
	tmp[0] = (unsigned char) ((len_int >> 8) & 0xff);
	tmp[1] = (unsigned char) (len_int & 0xff);


	/* Bitwise operation to set command (b) in header    */
	cmd_tmp = command;
	cmd_tmp <<= 4;
	tmp[0] |= cmd_tmp;

	/* Bitwise operation to set flag (a) in header    */
	flag <<= 7;
	tmp[0] |= flag;
#ifdef DEBUG
	printf("encoded header: tmp[0]: %d, tmp[1]: %d\n", (int) tmp[0], (int) tmp[1]);
#endif


	int i = 0;


	/*                Progress bar                   */
	if(total_str_len > 4000) progressbar((pcnt += len_int),total_str_len);

	/*                 Write-loop                    */
	while(1){
		tmp[i + 2] = msg[i];
		if(msg[i] == '\0') { // End of message
			tmp[0] &= ~(1 << 7); // Sets flag in header to 0
			flag = 0;
			write(sock, (void *) &tmp, len_int + 2);
			pcnt = 0;
			return 0;
		} else if (i == MAX_MSG){
			write(sock, (void *)&tmp, len_int + 2);
			if(flag){
				/*            Recursive call with rest of string           */
				sndmsg(sock, command, (char *) &msg[MAX_MSG], total_str_len);
			}
			break;
		}
		i++;
	}
	return 0;
}
