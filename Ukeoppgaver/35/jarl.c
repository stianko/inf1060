#include <stdio.h>
#include <string.h>
 
char strgetc(char s[], int pos){
  char thisChar = s[pos];
  return thisChar;
}
 
int main(int argv, char* argc[]){
  char typedString[50];
  int charPos;
  printf("Type in something\n");
  scanf("%s", typedString);
  printf("Type in which character you want to extract. Max: %zu\n", strlen(typedString));
  scanf("%d", &charPos);
  char thisChar = strgetc(typedString, --charPos);
  printf("The character on pos %d is %c\n", ++charPos, thisChar);
}
