#include <stdio.h>

int main() {
  short a, b, sum;

  a = -20000;  b = -20000;  sum = a+b;
  
  // tar ikke høyde for negative tall
  //if (sum < a || sum < b){
  //  printf("Overflow\n");
  //}

  if(sum - a != b){
    printf("OVERFLOW!!!!\n");
  }

  printf("%d + %d = %d\n", a, b, sum);
}

// En short er en 16-bit integer datatype som kan inneholde verdier i området −32,768 til 32,767
// for å få "riktig" verdi her må datatypen endres til unsigned
