#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char strgetc(char s[], int pos){
  char charOut = s[pos];
  return charOut;
}

int main (void){
  char str[100];
  int pos;
  //char char1 = strgetc(str, pos);
  printf("Skriv en setning: ");
  fgets(str, 100, stdin);
  printf("Skriv inn et tall: ");
  scanf("%d", &pos);
  printf("Tegn paa posisjon %d: %c", pos, strgetc(str, pos));
}
