#include <stdlib.h>
#include <stdio.h>

int ishex(unsigned char c){
  return((c >= 48 && c <= 57)||
	 (c >= 'A' && c <= 'F')||
	 (c >= 'a' && c <= 'f'));
}

int hexval(unsigned char c){
  if (ishex(c)){
    if (c >= 48 && c <= 57){
      return c - 48;
    } else if (c >= 65 && c <= 70){
      return c - 55;
    } else {
      return c - 87;
    }
  }
  return 0;
}

int main(int argc, char* argv[]){
  char c = '8';
  printf("%c i hex er: %d", c, hexval(c));
}
