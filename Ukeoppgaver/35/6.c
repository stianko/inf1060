#include <stdio.h>

int strcmpx(unsigned char s1[], unsigned char s2[]) {
  int i = 0;
  int i1 = 0;
  int i2 = 0;
  while(s1[i]){
    if(s1[i] != s2[i]){
      return s1[i] - s2[i];
    }
    i++;
  }
  return s1[i] - s2[i];

  
}

void test(unsigned char s1[], unsigned char s2[]) {
  printf("strcmpx(\"%s\", \"%s\") gir %d\n",
	 s1, s2, strcmpx(s1,s2));
}

int main() {
  test("Abc", "Ab");
  test("ababa", "babab");
  test("Abc", "Abe");
  test("Abc", "Abcd");
  test("Stian", "Naits");
  return 0;
}
