#include <stdio.h>
#include <string.h>
#define STREQ(s1,s2) (strcmp(s1,s2) == 0)
#define PLUS1(x) ++x

int main(void){
  char s[] = "Abc";
  char s2[] = "Abc";  
  
  if(STREQ(s,s2)){
    printf("TRUE");
  } else {
    printf("FALSE");
  }
}
