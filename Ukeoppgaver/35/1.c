#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int isNumber(char *msg){
  printf("Argumentet er et tall:\n");
  int testInt = atol(msg);
  float testFloat = atof(msg);
  
  if (testFloat > testInt){
    printf("Float: %f\n", testFloat);
  } else {
    printf("Int: %d\n", testInt);
  }
}

int main( int argc, char *argv[] ){
  char msg[100];
    
    if (argc == 1){
      printf("FEILMELDING: For få argumenter!");
    }
    else {
      strcpy(msg, argv[1]);
      strcat(msg, " ");
      if(argc > 1)
	{
	  int i;
	  for(i = 2; i < argc; i++)
	    { 
	      strcat(msg, argv[i]);
	      strcat(msg, " ");
	    }
	}
    }
    if(atoi(msg) == 0)
      {
	printf("%s\n", msg);
      } else {
      isNumber(msg);
      //return 0;
    }
}
