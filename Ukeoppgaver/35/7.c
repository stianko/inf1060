#include <stdio.h>
#include <stdlib.h>

int main(void){
  int inches = 1;
  while(inches != 0){
    printf("Inches: ");
    scanf("%d", &inches);
    if(inches != 0)
      printf("%d inches is %d foot and %d inches\n",
	     inches,
	     inches / 12,
	     inches % 12);
  }
  exit(0);
}
