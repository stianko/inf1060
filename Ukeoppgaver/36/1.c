#include <stdio.h>

int main(void){
  int a = 42;
  int b = 108;
  int* x = &a;
  int* y = &b;
  
  printf("a er %d\n", a);
  
  printf("x peker på verdien %d\n", *x);
  printf("y peker på verdien %d\n", *y);
 
  printf("Setter y til å peke på det samme som x\n");
  y = x;

  printf("x peker på verdien %d\n", *x);
  printf("y peker på verdien %d\n", *y);
}
