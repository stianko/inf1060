#include <stdlib.h>
#include <stdio.h>
#include <string.h>

FILE * fIn;
FILE * fOut;

int readUncompressedFile(char filename[]){
  fIn = fopen(filename,"r");
  char c;
  
  while((c = fgetc(fIn)) != EOF){
    printf("%c",c);
  }
  
  fclose(fIn);
  return 0;
}

int encode(char fileIn[],char fileOut[]){
  fIn = fopen(fileIn,"r");
  fOut = fopen(fileOut,"w");

  char c;
  char result = 0;
  char tmp;
  int bitcnt = 0;

  //while((c = fgetc(fIn)) != EOF){
  while(1){
    c = fgetc(fIn);
    if(c == ' ') tmp = 0;
    else if (c == ':') tmp = 1;
    else if (c == '@') tmp = 2;
    else if (c == '\n') tmp = 3;

    if(bitcnt == 0){
      tmp <<= 6;
      result |= tmp;
    }

    if(bitcnt == 1){
      tmp <<= 4;
      result |= tmp;
    }

    if(bitcnt == 2){
      tmp <<= 2;
      result |= tmp;
    }

    if(bitcnt == 3) result |= tmp;

    if(++bitcnt == 4){
      fwrite(&result,1,1,fOut);
      bitcnt = 0;
      result = 0;
    }
    if(c == EOF) break;
  }

  fclose(fIn);
  fclose(fOut);
  return 0;
}

int decode(char fileIn[]){
  fIn = fopen(fileIn,"r");
  int bitcnt;
  unsigned char result = 0;
  char c;

  //  while((c = fgetc(fIn)) != EOF){
  while(1){
    c = fgetc(fIn);
    for(bitcnt = 0;bitcnt < 4;bitcnt++){
      result = c;
      
      if(bitcnt == 0)result >>= 6;

      if(bitcnt == 1){
	result <<= 2;
	result >>= 6;
      }

      if(bitcnt == 2){
	result <<= 4;
	result >>= 6;
      }

      if(bitcnt == 3){
	result <<= 6;
	result >>= 6;
      }
      
      if(result == 0) printf(" ");
      if(result == 1) printf(":");
      if(result == 2) printf("@");
      if(result == 3) printf("\n");
    }
    if(c == EOF) break;
  }
  return 0;
}

int main(int argc, char* argv[]){
  if(argc > 2 && argc < 5){
    if(strcmp("p",argv[1]) == 0){
      readUncompressedFile(argv[2]);
    } else if(strcmp("e",argv[1]) == 0){
      encode(argv[2],argv[3]);
    } else if (strcmp("d",argv[1]) == 0){
      decode(argv[2]);
    }
  } else {
    printf("USAGE: ./a.out command input_file output_file\nwhere \"command\" is one of the following:\n   p    print input_file\n        (output_file is ignored if specified)\n   e    encode/compress input_file to output_file\n   d    decode/uncompress and print input_file\n        (output_file is ignored if specified)\n");
  }
  return 0;
}
