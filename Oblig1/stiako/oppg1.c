#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

/*
 * Oblig 1 - INF1060
 *
 * Oppgave 1 - Tekststrenger
 *
 * Stian Kongsvik (stiako)
 *
 */

// String compare macro
#define STREQ(s1,s2) (strcmp(s1,s2) == 0)

const char VOWELS[] = "aeiouyæøå";

int numberOfLines = 0;

// Nodes for linked list of text-lines
struct node{
  char stringLine[255];
  struct node* next;
};

// Takes a linked list from struct node
// and prints to terminal
int printFile(struct node *linkedList){
  while(1){
    printf("%s",linkedList->stringLine);
    if(linkedList->next == 0) break;
    else {
      linkedList = linkedList->next;
    }
  }
  return 0;
}

// Prints the string from a random node in the
// linked list
int printRandomLine(struct node *linkedList){
  int low = 1;
  int high = numberOfLines;
  time_t seconds;
  time(&seconds);
  srand((unsigned int) seconds);
  int randomLine = rand() % (high - low + 1) + low;
  int i = 1;
  while(1){    
    if(i == randomLine){
      printf("%s",linkedList);
      break;
    } else {
      linkedList = linkedList->next;
      i++;
    }
  }
  return 0;
}

// Either replaces all vowels with all single vowels
// or removes all vowels based on "remove"-parameter.
// Will correctly remove all vowels, but has problems with
// replacing "æøå". When replacing these, prints two vowels in place.
// Also prints wrong symbol when inserting "æøå".
// Uses UTF-8
int replaceVowels(struct node *linkedList, int iterations, int remove){
  int vowelIndex = 0;
  int lineIndex;
  char *line;

  struct node *root;
  root = linkedList;

  for(vowelIndex;vowelIndex<(iterations - 1);vowelIndex++){

    // Hack to not print æøå twice
    if(vowelIndex > 5) vowelIndex++;

    // Only prints the vowel that will be printed in the output
    // if function is called as replace
    if(!remove) printf("%c:\n",VOWELS[vowelIndex]);
    
    // Iterates through each char in the strings in the linked list.
    // All chars sent to function checkVowel.
    // Prints replacements or removes vowels based on function parameters.
    while(1){
      lineIndex = 0;
      for(lineIndex;lineIndex<sizeof(linkedList->stringLine);lineIndex++){
	if(!checkVowel(linkedList->stringLine[lineIndex])){
	  printf("%c",linkedList->stringLine[lineIndex]);
	} else if(linkedList->stringLine[lineIndex] == '\n' 
		  || linkedList->stringLine[lineIndex] == 0) break;
	else if (!remove){
	  printf("%c",VOWELS[vowelIndex]);
	}
      }
      if(linkedList->next == 0){
	linkedList = root;
	break;
      }
      linkedList = linkedList->next;
    }
  }
  return 0;
}

// Returns 1 if letter is a vowel, not case sensitive
int checkVowel(char *letter){
  char str[2];
  str[0] = letter;
  str[1] = 0;
  if(strcasestr(VOWELS,str)) return 1;
  return 0;
}

// Reads file, each line is placed as a string in each node of
// the linked list that is returned.
struct node *readFile(char filename[]){
  FILE * fs;
  fs = fopen(filename,"r");

  char buffer[255];

  struct node *firstLine;
  struct node *nextLine;
  firstLine = (struct node *) malloc(sizeof(struct node));
  firstLine->next = 0;
  nextLine = firstLine;

  int count = 0;
  while(1){
    if(fgets(buffer, 255, fs) != NULL){
      strcpy(nextLine->stringLine, buffer);
      numberOfLines++;
      nextLine->next = malloc(sizeof(struct node));
      nextLine = nextLine->next;
      } else {
      nextLine->next=0;
      break;
    }
  }
  fclose(fs);
  return firstLine;
}

// Traverses the linked list and counts number of chars.
// Prints to terminal
int charCount(struct node *linkedList){
  int counter = 0;
  while(1){
    if(linkedList->next == NULL) break;
    int i = 0;

    for(i;i<sizeof(linkedList->stringLine);i++){
      counter++;
      if(linkedList->stringLine[i] == 0 || linkedList->stringLine[i] == '\n') break;
    }

    linkedList = linkedList->next;
  }
  printf("%d: \n",counter);
  return 0;
}

// Recursive function to free memory.
int freeMem(struct node *linkedList){
  if(linkedList->next != NULL) freeMem(linkedList->next);
  free(linkedList);
  return 0;
}

// Arguments: ./a.out command file_name
int main(int argc, char* argv[]){

  char command[24];
  char filename[64];
  if(argc != 3){
    fprintf(stderr, "Usage:\n./a.out command filename\n");
    exit(EXIT_FAILURE);
  } else {

    // Reads file, sends linked list as parameters to functions
    strcpy(command, argv[1]);
    strcpy(filename, argv[2]);
    struct node *linkedList = readFile(filename);
    
    if(STREQ(command,"print")){
      printf("Print file: %s\n", filename);
      printFile(linkedList);
    } else if(STREQ(command,"random")){
      printf("Print a random line\n");
      printRandomLine(linkedList);
    } else if(STREQ(command,"replace")){
      printf("Replace all vowels with all other vowels\n");
      replaceVowels(linkedList,sizeof(VOWELS),0); 
    } else if(STREQ(command,"remove")){
      printf("Remove vowels\n");
      replaceVowels(linkedList,2,1);
    } else if(STREQ(command,"len")){
      printf("Print the number of characters in the input_file\n");
      charCount(linkedList);
    } else {
      printf("Feil, %s", argv[2]);
      exit(EXIT_FAILURE);
    }
    freeMem(linkedList); // Frees allocated memory
  }
  return 0;
}
