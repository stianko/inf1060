#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>

#define MAX_LENGTH 120
#define MAX_PARAM 21

pid_t safefork(void);
int freeparam(char *param[]);
int cmd_exec(char *param[]);
int fork_flag = 0;
int history_init(void);
int history(void);
int add_to_history(char *inputstr);
void print_bitmap(void);
void exec_history(char *param[]);
void free_history(void);

