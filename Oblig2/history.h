#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define MAX_LENGTH 120
#define MAX_PARAM 21

struct metadata{
	struct metadata *next;
	int length;
	char data[15];
};

int history_init(void);
int bitmap_init(void);
int add_to_history(char *inputstr);
int delete_history(int index);
int is_allocated(int index);
int history(void);
void allocate_bit(int index);
void deallocate_bit(int index);
void print_bitmap(void);
int add_recursive(char *inputstr, struct metadata *block);
void exec_history(char *param[]);
int cmd_split(char *inputstr,char *param[]);
void free_history(void);
void free_rec(struct metadata *block);
void delete_oldest(void);
