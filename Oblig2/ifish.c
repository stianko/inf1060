#include "ifish.h"
/*------------------------------------------
 *
 *            Oblig 2 - INF1060
 *
 *           Stian Kongsvik (stiako)
 *
 *------------------------------------------
 */

// Splits inputstr and inserts each string in the array param.
int cmd_split(char *inputstr,char *param[]){
	int paramcnt = 0;
	char delimiters[] = " \n";
	int bitmap_init(void);
	int bitmap_init(void);
	char *token;


	// Separates string and copies each part to the parameter list
	while(1){
		token = strsep(&inputstr, delimiters);
		if(token == NULL){
			param[paramcnt] = NULL;
			break;
		}
		// Sets a flag that tells cmd_exec to fork out the process
		// Does not work
		if(!strcmp(token,"&")){
			param[paramcnt] = NULL;
			fork_flag = 1;
			break;
		}
		if(strcmp(token,"")){
			param[paramcnt] = malloc(strlen(token));
			strcpy(param[paramcnt++],token);
		}
	}

#ifdef DEBUG
	fprintf(stderr,"PARAM: \n");
	int i = 0;
	while(param[i] != 0){
		fprintf(stderr,"%d: |%s|\n",i,param[i]);
		i++;
	}
	fprintf(stderr,"%d: %s\n",i,param[i]);
#endif

	// Check for h, shell builtin
	// If command is h with parameters, h p1 p2 ... is sent to 
	// exec_history. exec_history calls this function back with
	// the stored inputstring from the history.
	if(strcmp(param[0],"h") == 0){
		if(param[1] == 0) history();
		else exec_history(param);
	} else cmd_exec(param);
	
	return 0;
}

// Frees all elements in param
int freeparam(char *param[]){
	int i = 0;

	while(param[i] != NULL){
#ifdef DEBUG
		fprintf(stderr,"Freeing pointer: %p\n",param[i]);
#endif
		free(param[i]);
		i++;
	}
	return 0;
}

// Executes command from the parameter list passed from cmd_split.
int cmd_exec(char *param[]){
	pid_t pid = -1;
	extern char **environ;
	char *path = getenv("PATH");
	char delimiters[] = ":";
	char *token;
	char *tmppath;

	// Checks if parent or child
	if((pid = safefork())){
#ifdef DEBUG
		printf("parent PID=%d\n", (int)getpid());
#endif
		// Not working as intended
		if(fork_flag) printf("PID: %d\n",pid);
		else wait(0);

	} else {
#ifdef DEBUG
		printf("child PID=%d\n", (int)getpid());
		printf("----------------- execve ----------------\n");
#endif
		// Iterates through all paths and tries to exec param[]
		while(1){
			token = strsep (&path, delimiters);
			if(token == NULL) break;
			
#ifdef DEBUG
			printf("%s\n",token);
#endif
			tmppath = strdup(token);
			strcat(tmppath,"/");	
			strcat(tmppath,param[0]);
#ifndef DEBUG
			// Executes param[]
			execve(tmppath, param, environ);
#else
		int status = 0;
			status = execve(tmppath, param, environ);
			printf("execve returned %d:\n%s\n",status,strerror(errno));
#endif
		}
		printf("ifish: %s: command not found\n",param[0]);
		exit(0);
	}

	return 0;
}

// Main loop to give prompt
int cmd_loop(void){
	char *user;
	char inputstr[MAX_LENGTH];
	char *param[MAX_PARAM];
	int cmds = 0;
	user = getenv("USER");

	while(1){
#ifdef DEBUG
			char *cwd;
			cwd = getenv("PWD");
			fprintf(stderr,"\nDEBUG %s\n",cwd);
#endif
		printf("%s@ifish %d > ",user,cmds++);
		// Stores input and checks for EOT
		if(fgets(inputstr, MAX_LENGTH, stdin)==NULL){
			free_history();
			exit(0);
		}
#ifdef DEBUG
		fprintf(stderr,"Input string:\n%s\n",inputstr);
#endif
		// Checks for builtin commands for exiting
		if(strcmp(inputstr,"exit\n") == 0 || strcmp(inputstr,"quit\n") == 0){
			printf("%s has logged off\n",user);
			free_history();
			exit(0);
		}
		// Checks that input is not empty and removes newline.
		// Readies input to be sent to cmd_split and adds to history
		if(strcmp(inputstr,"\n") != 0){
			char inputcpy[strlen(inputstr)];
			strcpy(inputcpy,inputstr);
			inputcpy[strlen(inputcpy) - 1] = '\0';
			add_to_history(inputcpy);
			cmd_split(inputstr,param);
			
#ifdef DEBUG
			print_bitmap();
#endif
			// Frees parameter list
			freeparam(param); 
		}
	}
	return 0;
}

int main(void){
	printf("*------------------------------------*\n");
	printf("|                                    |\n");
	printf("|                ifish               |\n");
#ifdef DEBUG
	printf("|    the leaking & crashing shell    |\n");
#endif
	printf("|                                    |\n");
	printf("*------------------------------------*\n");
	history_init(); // Initializes history
	cmd_loop();
	return 0;
}
