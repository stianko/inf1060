#include "history.h"
// history_mem contains a 8*8 bitmap at index 0 for 1-64 of 
// the rest of the array
char *history_mem[65];
char *bitmap;
// Head is the firsts element of the metadata linked list
struct metadata *head;
int history_length = 0;

// Initalizes memory for history
int history_init(void){
	bitmap_init();
	head = (struct metadata *) malloc(sizeof(struct metadata));
	head->next = NULL;
	return 0;
}

// Initializes bitmap
int bitmap_init(void){
	bitmap = history_mem[0];
	bitmap = calloc(8,sizeof(char));
	return 0;
}

// Adds a string to history
int add_to_history(char *inputstr){
	int length = strlen(inputstr);
	
	//Add metadatablock to metadata linked list
	struct metadata *block = (struct metadata *) malloc(sizeof(struct metadata));
	block->next = head->next;
	head->next = block;
	block->length = length;
	// Global variable to keep track of number of elements
	history_length++;
	// Calls a recursive function that stores commands
	add_recursive(inputstr, block);
	return 0;
}

// Recursive function that stores commands in 8 byte blocks.
// Calls itself when command is longer than 8 bytes. 
int add_recursive(char *inputstr, struct metadata *block){
	int i = 0;
	int j;
	int max;
	int length = strlen(inputstr);
	char substr[MAX_LENGTH];

	// Checks size of command
	if(length > 8) max = 8;
	else max = length + 1;

	while(1){
		if(!is_allocated(i)){
			//Update bitmap
			allocate_bit(i);
			history_mem[i + 1] = malloc(max);
			//Copy substring to datablock
			for(j = 0;j < max; j++){
				history_mem[i + 1][j] = inputstr[j];
			}
			// Adds indexes of memory blocks to metadata data array
			j = 0;
			while(1){
				if(block->data[j]==0){
					block->data[j] = i + 1;
					break;
				}
				j++;
			}
			break;
		}
		// Starts deleting oldest blocks if full.
		i++;
		if(i == 64){
			i = 0;
			delete_oldest();
		}
	}
	// Sends subtring without first 8 chars to next recursion
	if(length > 8){
		strcpy(substr,&inputstr[8]);
		add_recursive(substr, block);
	}
	return 0;
}

// Prints bitmap and memory blocks
// Used for debug
void print_bitmap(void){

	// Prints bitmap
	int i;
	for(i = 0; i < 64; i++){
		if(i == 32) printf("\n");
		fprintf(stderr,"%d",is_allocated(i));
	}
	printf("\n");

	// Prints memory blocks based on bitmap
	int j;
	int flag = 0;
	for(i = 1; i < 65; i++){
		fprintf(stderr,"##");
		if (is_allocated(i - 1)){
			for(j = 0; j < 8; j++){
				//printf("%d",flag);
				if(history_mem[i][j] == '\0') flag = 1;
				if(flag) fprintf(stderr,".");
				else fprintf(stderr,"%c",history_mem[i][j]);
			}
		} else fprintf(stderr,"        ");
		flag = 0;
		if(!(i % 4)) fprintf(stderr,"##\n");
	}

}

// Returns value of bit at index (0-63) of bitmap
int is_allocated(int index){
	return (bitmap[(index / 8) % 8] >> (index % 8)) & 1;
}

// Sets value of bit at index (0-63) of bitmap to 1
void allocate_bit(int index){
	bitmap[(index / 8) % 8] |= (1 << (index % 8));
}

// Sets value of bit at index (0-63) of bitmap to 0
void deallocate_bit(int index){
	bitmap[(index / 8) % 8] &= ~(1 << (index % 8));	
}

// Copies the string stored in blocks to string that is passed in
// the parameter
void block_strcpy(struct metadata *block, char *dest){
	char str[block->length + 1];
	int i;
	// Builds strings from memory blocks
	for(i = 0; i < block->length; i++){
		  str[i] = history_mem[(unsigned)block->data[(i / 8) % 8]][i % 8];
	}
	str[block->length] = '\0';
	// Copies string to string passed in parameter
	strcpy(dest,str);
}

// Recursive call that prints history with oldest
// element at the top
int tmp_index;
void history_rec(struct metadata *block){
	if(block->next) history_rec(block->next);
	char str[block->length + 1];
	block_strcpy(block,str);
	printf("%d: %s\n",tmp_index--,str);
}

// Prints string stored in a single metadata block
void print_block(struct metadata *block){
	char str[block->length + 1];
	block_strcpy(block,str);
	printf("%s",str);
}

// Executes previous commands in history based on parameters
void exec_history(char *param[]){
	int index;
	int i = 0;
	char *param1[MAX_PARAM];
	struct metadata *block = head->next;

	// Prints history if no parameters are passed for h
	if(param[1] == NULL){
		history();
		return;
	// Deletes history items if parameter -d is passed
	} else if(strcmp(param[1],"-d") == 0){
		delete_history(atoi(param[2]));
		return;
	}
	else index = atoi(param[1]);

	if(index > history_length){
		printf("History item %d does not exist\n", index);
		return;
	}
	
	for(i = 0; i < index; i++){
		block = block->next;
	}
	
	char cmd[block->length + 1];
	// Copies string from block and passes back to cmd_split
	// in ifish that executes command. Possible infinite loop
	// if calling a command that calls itself back
	block_strcpy(block,cmd);
	cmd_split(cmd, param1);
}

// Deletes a node/block in the metadata linked list based on 
// the index. Frees the memory
int delete_history(int index){
	int i;
	int j = 0;
	struct metadata *block = head->next;
	struct metadata *prev_block;
	for(i = 0; i < index - 1; i++){
		block = block->next;
	}
	prev_block = block;

	block = block->next;
	prev_block->next = block->next;
	
	while(1){
		if(block->data[j]){
		deallocate_bit(block->data[j] - 1);
		free(history_mem[(unsigned)block->data[j]]);
		j++;
		} else break;
	}
	free(block);
	history_length--;
	return 0;
}

// Deletes the oldest block in the metadata linked list
// Also used to free memory when exiting
void delete_oldest(){
	struct metadata *block = head;
	int i = 0;
	while(block->next){
		block = block->next;
		i++;
	}
#ifdef DEBUG
	printf("Deleting block number: %d\n",(i - 1));
#endif
	delete_history(i - 1);
}

// Prints history
int history(void){
	printf("History list of the last %d commands\n",history_length);
	struct metadata *block = head->next;
	tmp_index = history_length;
	// Recursive call to print history in chronological order
	history_rec(block);
	return 0;
}

// Frees history using the delete_oldest function
void free_history(void){
	while(1){
		if(head->next->next == NULL) break;
		delete_oldest();
	}
	free_final();
#ifdef DEBUG
	print_bitmap();
#endif
	free(head);
	free(bitmap);
}
// Frees the newest element
void free_final(void){
	struct metadata *block = head->next;
	int j = 0;
	while(1){
		if(block->data[j]){
		deallocate_bit(block->data[j] - 1);
		free(history_mem[(unsigned)block->data[j]]);
		j++;
		} else break;
	}
	free(block);
	history_length--;
}
